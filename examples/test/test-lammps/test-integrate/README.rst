test-integrate
==============

This test is indended to check that we can integrate the
equations of motion using LAMMPS.

The test is run with:

python test_lammps.py

If you want to graphically show the comparisons, this can be done
by running the test with:

python test_lammps.py plot
