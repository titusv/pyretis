Orderparameter
--------------
class = OrderParameter
name =  test

Collective-variable
-------------------
class = Position
name = Position
index = 0
dim = x
periodic = False

Collective-variable
-------------------
class = Velocity
name = Velocity
index = 0
dim = x

Collective-variable
-------------------
class = PositionVelocity
name = PositionVelocity
index = 0
dim = x
periodic = True

Collective-variable
-------------------
class = Distance
name = My distance
index = (100, 101)
periodic = False

Collective-variable
-------------------
class = Distancevel
name = My distance Velocity
index = (102, 103)
periodic = True

Collective-variable
-------------------
class = DistanceVelocity
name = My distance Position Velocity
index = (1001, 1002)
periodic = True

Collective-variable
-------------------
class = Angle
name = Angle
index = (1001, 1002, 1003)
periodic = True

Collective-variable
-------------------
class = Dihedral
name = Dihedral
index = (1001, 1002, 1003, 1004)
periodic = True
